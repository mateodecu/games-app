import React, { Fragment } from 'react'
import { getGames } from '../../selectors/getGames'
import { GameCard } from '../GameCard/GameCard';
import './GamesList.css'

export const GamesList = () => {
  const games = getGames();

  return (
    <Fragment>
      <h1 className='games-list-title'>BESTSELLERS</h1>
      <div className="cards-section">
        {
          games.map((game, i) => (
            <GameCard key={i} id={game.id} />
          ))
        }
      </div>
    </Fragment>
  )
}
