import { Fragment } from 'react';
import { useParams } from 'react-router-dom';
import { getGameById } from '../../selectors/getGameById';
import { getGameImgSrcById } from '../../selectors/getGameImgSrcById';
import './Game.css';

export const Game = () => {
    const {gameId} = useParams()
    const game = getGameById(gameId || '');
    const gameImgSrc = getGameImgSrcById(game?.id);

    const getImgStyle = (gameImgSrc: any) => ({
        'width': '100%',
        'height': '450px',
        'backgroundSize': 'cover',
        'backgroundImage': gameImgSrc ? ("url('../../../public/" + gameImgSrc + "')") : "url('../../../public/assets/generic-game.jpg'",
    })

    return (
        <Fragment>
            <div style={getImgStyle(gameImgSrc)}></div>
            <div className='game-data'>
                {game?.new_release && <div className='new-release'>
                    <img src='../assets/alert.png' alt='NEW RELEASE' className='new-release-alert' />
                    <span className='new-release-text'>NEW RELEASE</span>
                </div>}
                <div className='game-title'>{game?.name}</div>
                <div className='game-install-bar'>
                    <div className='price'>{game?.price} €</div>
                    <button className='install-button'>INSTALL GAME</button>
                </div>
                <div className='game-info-bar'>
                    <img src={gameImgSrc} alt={game?.name} className='game-info-image' />
                    <span className='game-info'>{game?.description}</span>
                </div>
            </div>
        </Fragment>
    )
}
