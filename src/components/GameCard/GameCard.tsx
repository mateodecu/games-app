import { Fragment } from 'react';
import { Link } from "react-router-dom";
import { getGameById } from '../../selectors/getGameById';
import { getGameImgSrcById } from '../../selectors/getGameImgSrcById';
import './GameCard.css';

export const GameCard = (props: any) => {
    const game = getGameById(props.id);
    const gameImgSrc = getGameImgSrcById(props.id);

    return (
        <Fragment>
            <div className='game-card'>
                <img src={gameImgSrc} alt={game?.name} className='game-card-img' />
                <div className="game-card-foot">
                    <div className='game-card-details'>
                        <div className="game-card-name">{game?.name}</div>
                        <div className="game-card-price">{game?.price} USD</div>
                    </div>
                    <Link to={`/game/${game?.id}`}>
                        <button className="game-card-buy-button">BUY NOW</button>
                    </Link>
                </div>
            </div>
        </Fragment>
    )
}
