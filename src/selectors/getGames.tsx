import { games } from '../data/games';
import { Game } from '../models/Game';

export const getGames = (): Game[] => {
    return games;
};
