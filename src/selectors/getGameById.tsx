import { games } from '../data/games';
import { Game } from '../models/Game';

export const getGameById = (id: string): Game | undefined => {
    return games.find(game => game.id === id);
};
