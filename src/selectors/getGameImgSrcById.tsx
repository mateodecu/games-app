export const getGameImgSrcById = (id: string | undefined): string => {
    return '../assets/' + id + '.jpg';
};
