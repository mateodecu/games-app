export interface Game {
    id: string,
    name: string,
    price: number,
    new_release: boolean,
    description: string,
}