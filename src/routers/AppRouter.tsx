import { Routes, Route, BrowserRouter } from "react-router-dom";
import { Game } from '../components/Game/Game';
import { GamesList } from '../components/GamesList/GamesList';

export const AppRouter = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<GamesList />} />
                <Route path='/gamesList' element={<GamesList />} />
                <Route path='/game/:gameId' element={<Game />} />
            </Routes>
        </BrowserRouter>
    )
}
