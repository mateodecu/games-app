import ReactDOM from 'react-dom/client';
import './index.css';
import { GamesApp } from './GamesApp';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(<GamesApp />);
