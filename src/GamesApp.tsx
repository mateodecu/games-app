import { AppRouter } from './routers/AppRouter';
import './GamesApp.css'

export const GamesApp = () => {
    return (
        <AppRouter />
    );
};
